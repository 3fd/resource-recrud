import * as http from 'http-error-handler';
import { NextFunction, Request, Response} from 'express';
import expressValidator = require('express-validator');
import { Document, Model, ModelPopulateOptions } from 'mongoose';

export interface ResourceValidation {
  name: string;
  type: string;
}

export interface ResourceDescription {
  defaultFilter?: {};
  identifier?: string;
  populations?: ModelPopulateOptions[] | string ;
  selection?: string;
  sortableBy?: string[];
  sortDefault?: string;
  validations?: ResourceValidation[];
}

const defaultDescription: ResourceDescription = {
  populations: '',
  selection: '-__v',
  sortableBy: ['createdAt'],
  sortDefault: '-createdAt',
};

const validate = (validations: ResourceValidation[] = [], req: Request) => {
  validations.forEach((path) => {
    const { name, type } = path;
    switch (type) {
      case 'Alpha':
        req.check(name, name + ' must be an alpha string').optional().isAlpha(); break;
      case 'Alphanumeric':
        req.check(name, name + ' must be an alpha numeric string').optional().isAlphanumeric(); break;
      case 'Boolean':
        req.check(name, name + ' must be a boolean').optional().isBoolean(); break;
      case 'Email':
        req.check(name, name + ' must be an email address').optional().isEmail(); break;
      case 'Empty':
        req.check(name, name + ' must not be set').isEmpty(); break;
      case 'Float':
        req.check(name, name + ' must be an integer').optional().isFloat(); break;
      case 'Int':
        req.check(name, name + ' must be an integer').optional().isInt(); break;
      case 'Number':
        req.check(name, name + ' must be a number').optional().isDecimal(); break;
    }
  });
};

const deleteEmptyBodyParams = (validations: ResourceValidation[] = [], body: any) => {
  validations.forEach((path) => {
    if (path.type === 'Empty') {
      delete body[path.name];
    }
  });
};

export let create = (Resource: Model<Document>, description: ResourceDescription = defaultDescription) => {
  return (req: Request, res: Response, next: NextFunction) => {
    Resource.schema.requiredPaths().forEach((path) => {
      req.checkBody(path, 'Parameter missing').notEmpty();
    });
    Resource.schema.eachPath((path, type) => {
      switch ((type as any).instance) {
        case 'Boolean':
          req.checkBody(path, path + ' must be a boolean').optional().isBoolean(); break;
        case 'Number':
          req.checkBody(path, path + ' must be a number').optional().isDecimal(); break;
      }
    });
    validate(description.validations, req);
    deleteEmptyBodyParams(description.validations, req.body);

    req.getValidationResult()
      .then((result) => {
        result.throw();
        const resource = new Resource(req.body);
        return resource.save();
      })
      .then((resource) => res.status(201).json(resource))
      .catch(next);
  };
};

export let read = (Resource: Model<&Document>, description: ResourceDescription = defaultDescription) => {
  return (req: Request, res: Response, next: NextFunction) => {
    if (description.identifier) {
      req.checkParams('id', 'Parameter missing').notEmpty();
    }
    else {
      req.checkParams('id', 'Parameter missing or invalid. Must be ObjectId').notEmpty().isMongoId();
    }
    validate(description.validations, req);
    const populations = req.query.populate === 'true' ? description.populations ? description.populations : '' : '';

    const identifier = description.identifier || '_id';
    req.getValidationResult()
      .then((result) => {
        result.throw();
        return Resource
          .findOne({[identifier]: req.params.id})
          .populate(populations)
          .select(description.selection || '')
          .catch((err) => { throw http.internalError; });
      })
      .then((resource: &Document) => {
        if (resource) {
          return res.status(200).json(resource);
        }
        else {
          const notFound = { ...http.notFound, message: Resource.modelName + ' could not be found' };
          return res.status(notFound.code).json(notFound);
        }
      })
      .catch(next);
  };
};

export let readAll = (Resource: Model<&Document>, description: ResourceDescription = defaultDescription) => {
  return (req: Request, res: Response, next: NextFunction) => {
    if (req.query.sort && (description.sortableBy || []).indexOf(req.query.sort) === -1) {
      const badRequest = { ...http.badRequest, message: 'Can not sort by ' + req.query.sort };
      return http.errorHandler(badRequest, req, res);
    }
    const sortField = req.query.sort ? req.query.sort : description.sortDefault;
    const sortOrder = req.query.order === 'DESC' ? '-' : '';
    const skip = req.query.skip ? +req.query.skip : 0;
    const limit = req.query.limit ? +req.query.limit : Number.MAX_SAFE_INTEGER;
    const all = req.query.all === 'true' ? {} : description.defaultFilter ? description.defaultFilter : {};
    const populations = req.query.populate === 'true' ? description.populations ? description.populations : '' : '';

    Resource
      .find(all)
      .skip(skip)
      .limit(limit)
      .sort(sortOrder + sortField)
      .populate(populations)
      .select(description.selection || '')
      .then((resources: &Document[]) => {
        res.set('X-Total-Count', String(resources.length));
        return res.status(200).json(resources);
      })
      .catch(next);
  };
};

export let update = (Resource: Model<&Document>, description: ResourceDescription = defaultDescription) => {
  return (req: Request, res: Response, next: NextFunction) => {
    if (description.identifier) {
      req.checkParams('id', 'Parameter missing').notEmpty();
    }
    else {
      req.checkParams('id', 'Parameter missing or invalid. Must be ObjectId').notEmpty().isMongoId();
    }
    validate(description.validations, req);

    const identifier = description.identifier || '_id';
    req.getValidationResult()
      .then((result) => {
        result.throw();
        return Resource
          .findOne({[identifier]: req.params.id})
          .catch((err) => { throw http.internalError; });
      })
      .then((resource: &Document) => {
        if (resource) {
          return Resource.update({[identifier]: resource._id}, {$set: req.body});
        }
        else {
          throw { ...http.notFound, message: Resource.modelName + ' could not be found'};
        }
      })
      .then(() => res.status(200).end())
      .catch(next);
  };
};

export let destroy = (Resource: Model<&Document>, description: ResourceDescription = defaultDescription) => {
  return (req: Request, res: Response, next: NextFunction) => {
    if (description.identifier) {
      req.checkParams('id', 'Parameter missing').notEmpty();
    }
    else {
      req.checkParams('id', 'Parameter missing or invalid. Must be ObjectId').notEmpty().isMongoId();
    }
    validate(description.validations, req);

    const identifier = description.identifier || '_id';
    req.getValidationResult()
      .then((result) => {
        result.throw();
        return Resource
          .remove({[identifier]: req.params.id})
          .catch((err) => { throw http.internalError; });
      })
      .then(() => {
        return res.status(200).end();
      })
      .catch(next);
  };
};
